import {
  ActionReducerMap
} from '@ngrx/store';
import { todoListFeatureKey, todoListReducer, TodoListState } from '@todo-ngrx/todos.reducer';


export interface AppState {
  [todoListFeatureKey]: TodoListState,
  // counterState: CounterState
}

export const reducers: ActionReducerMap<AppState> = {
  [todoListFeatureKey]: todoListReducer,
  // counterState: counterReducer
};
