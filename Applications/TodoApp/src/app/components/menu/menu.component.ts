import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  public isCollapsed: boolean;

  public items: { label: string, route: string }[];

  constructor() { }

  ngOnInit(): void {
    this.items = [
      { label: 'Home', route: 'home' },
      { label: 'Todo List', route: 'todoList' },
      { label: 'Todo List with ngrx', route: 'todoListCopy' },
      { label: 'Counter with ngrx', route: 'counter' }
    ];
  }
}
