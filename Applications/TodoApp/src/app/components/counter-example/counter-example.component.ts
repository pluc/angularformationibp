import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
@Component({
  selector: 'app-counter-example',
  templateUrl: './counter-example.component.html',
  styleUrls: ['./counter-example.component.scss']
})
export class CounterExampleComponent implements OnInit {

  @Input()
  public count: number;

  private _counterMessage: string;
  @Input()
  public get counterMessage(): string {
    return this._counterMessage;
  }
  public set counterMessage(value: string) {
    this._counterMessage = value;   
  }

  @Output()
  public countChange = new EventEmitter<number>();

  constructor() { }

  public increment(): void {
    if (this.count == null) {
      this.count = 0;
    }

    this.count++;
  }

  public sendCurrentCount(): void {
    this.countChange.emit(this.count);
  }

  ngOnInit(): void {
  }

}
