import { Observable } from 'rxjs';

export interface ICrudService<T> {

    GetAll(): Observable<T[]>;

    Create(item: T): Observable<boolean>;

    Update(item: T): Observable<boolean>;

    /**
     * La méthode Delete prends en paramètre un item.
     * De type T ou de type "number"
     */
    Delete(item: T | number): Observable<boolean>;

    Get(id: number): Observable<T>;
}
