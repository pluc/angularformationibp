import { CounterState } from '@counter/counter.state'
import * as CounterActions from '@counter/counter.actions';
import { AfterViewInit, Component, ElementRef, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit, AfterViewInit {

  @ViewChild('resetButton')
  public resetButton: ElementRef;

  public counterState$: Observable<CounterState>;

  // Injection d'un store ngrx
  constructor(private store: Store) {

    // On récupère le state via le reducer 'counter' 
    // (clé définie dans le module : StoreModule.forRoot({ counter: counterReducer }))
    this.counterState$ = store.select((s: any) => s.counter);
  }

  ngAfterViewInit(): void {
   console.info('ngAfterViewInit', this.resetButton);
  }

  ngOnInit(): void {
    console.info('ngOnInit', this.resetButton);
  }

  increment(): void {
    // Dispatch de l'action au store
    this.store.dispatch(CounterActions.increment());
  }

  decrement(): void {
    this.store.dispatch(CounterActions.decrement());
  }

  reset(): void {
    this.store.dispatch(CounterActions.reset());
  }
}
