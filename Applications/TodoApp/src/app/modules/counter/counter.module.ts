import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { counterReducer } from './counter.reducers';
import { CounterComponent } from './counter/counter.component';

@NgModule({
  declarations: [CounterComponent],
  exports: [CounterComponent],
  imports: [
    CommonModule,
    // On donne une clé pour chaque reducer. On a accès au reducer "counterReducer" via la clé "counter"
    StoreModule.forRoot({ counter: counterReducer })
  ]
})
export class CounterModule { }
