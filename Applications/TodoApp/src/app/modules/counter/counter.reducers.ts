import { Action, createReducer, on, } from '@ngrx/store';
import * as CounterActions from './counter.actions';
import { CounterState } from './counter.state';

// Définition d'un état initial
export const initialState = new CounterState(0);

// Définition du reducer, qui fait le mapping entre les actions et les états
const _counterReducer = createReducer(
    initialState,
    on(CounterActions.increment, (state: CounterState) => new CounterState(state.value + 1)),
    on(CounterActions.decrement, (state: CounterState) => new CounterState(state.value - 1)),
    on(CounterActions.reset, (state: CounterState) => new CounterState(0))
);

// Fonction reducer à importer dans le module adéquat
export function counterReducer(state: CounterState, action: Action) {
    return _counterReducer(state, action);
}
