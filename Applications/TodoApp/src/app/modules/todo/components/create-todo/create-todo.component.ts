import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Todo } from '../../models/todo';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.scss']
})
export class CreateTodoComponent implements OnInit {

  public todo: Todo;
  public hasErrors: boolean;

  constructor(private todoService: TodoService, private ngbActiveModal: NgbActiveModal) { }

  ngOnInit(): void {
    this.todo = new Todo(null, null, null);
  }

  public saveTodo(todo: Todo): void {
    this.todoService.Create(todo).subscribe(
      success => {
        this.hasErrors = false;
        if (this.ngbActiveModal) {
          this.ngbActiveModal.close();
        }
      },
      error => {
        this.hasErrors = true;
      },
      () => { }
    );
  }

}
