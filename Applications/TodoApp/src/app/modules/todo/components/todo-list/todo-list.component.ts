import { ChangeDetectionStrategy, Component, DoCheck, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, Subscription } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Person } from '../../models/person';
import { Todo } from '../../models/todo';
import { PersonService } from '../../services/person.service';
import { TodoService } from '../../services/todo.service';
import { CreateTodoComponent } from '../create-todo/create-todo.component';
import { UpdateTodoComponent } from '../update-todo/update-todo.component';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  changeDetection : ChangeDetectionStrategy.OnPush
})
export class TodoListComponent implements OnInit, DoCheck {

  public todoList$: Observable<Todo[]>;
  public message: string;
  private currentTimeout: any;
  public alertType: string;

  constructor(
    private todoService: TodoService,
    private personService: PersonService,
    private modalService: NgbModal) { }

  ngDoCheck(): void {
    console.info('TodoListComponent - DoCheck()');
  }

  ngOnInit(): void {
    console.log('Component TodoList initializing');
    // 1- On a un observable qui contient tous les todos
    // 2- On a un observable qui contient toutes les personnes
    // 3- => Créer un observable qui retourne les todos avec les personnes affectées
    this.todoList$ = this.todoService.GetAll().pipe(

      // mergeMap => permets de retourner un observable a exécuter à la suite d'un autre      
      mergeMap((todos: Todo[]) => {
        return this.personService.GetAll().pipe(
          // map => permets d'effectuer une opération synchrone à la suite d'un observable
          map((persons: Person[]) => {

            // boucle sur les todos pour aller affecter les personnes affectées sur les todos
            todos.map((t: Todo) =>
              // on récupére les personnes qui sont affectées au todo
              t.persons = persons.filter(p => (t.personsId || []).includes(p.id)));

            return todos;
          })
        )
      }),
      catchError((e, r) => {
        console.info('Error loading datas in TodoListComponent', e);
        return r;
      })
    );
  }

  public updateTodo(todo: Todo): void {
    this.todoService.Update(todo).subscribe(
      success => {
        console.info('Success event on Observable');
        this.showAlertMessage(success ? 'Enregistrement effectué avec succès' : `Erreur lors de l'enregistrement`, success);
      },
      error => {
        console.info('Error event on Observable', error);
        this.showAlertMessage(`Une erreur est survenue lors de l'enregistrement`, false);
      },
      () => {
        console.info('Complete event on Observable');
      });
  }

  public updateTodoInModal(todo: Todo): void {
    this.modalService.open(UpdateTodoComponent).componentInstance.todo = todo;
  }

  public removeTodo(todo: Todo): void {
    this.todoService.Delete(todo).subscribe(
      success => this.showAlertMessage(success ? 'Suppression effectuée avec succès' : `Erreur lors de la suppression`, success),
      error => this.showAlertMessage(`Erreur lors de la suppression`, false),
      () => { /* complete event */ }
    );
  }

  public addTodo(): void {
    this.modalService.open(CreateTodoComponent);
  }

  private showAlertMessage(s: string, success: boolean): void {
    this.message = s;

    if (this.currentTimeout) {
      clearTimeout(this.currentTimeout);
    }

    this.alertType = success ? 'success' : 'danger';

    this.currentTimeout = setTimeout(() => this.message = null, 2000);
  }
}
