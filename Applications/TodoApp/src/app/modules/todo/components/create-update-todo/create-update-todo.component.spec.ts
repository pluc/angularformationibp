import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateUpdateTodoComponent } from './create-update-todo.component';

describe('CreateUpdateTodoComponent', () => {
  let component: CreateUpdateTodoComponent;
  let fixture: ComponentFixture<CreateUpdateTodoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateUpdateTodoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateUpdateTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
