import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { Person } from '../../models/person';
import { Todo } from '../../models/todo';
import { PersonService } from '../../services/person.service';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-create-update-todo',
  templateUrl: './create-update-todo.component.html',
  styleUrls: ['./create-update-todo.component.scss']
})
export class CreateUpdateTodoComponent implements OnInit {

  @Input()
  public todo: Todo;

  @Input()
  public hasErrors: boolean;

  @Output()
  public saved = new EventEmitter<Todo>();

  public formGroup: FormGroup;
  public submitted: boolean;
  public personList$: Observable<Person[]>;

  constructor(private formBuilder: FormBuilder, private personService: PersonService) { }

  ngOnInit(): void {
    this.personList$ = this.personService.GetAll();

    this.formGroup = this.formBuilder.group({
      name: [this.todo.name, Validators.required],
      deadLineDate: [this.todo.deadLineDate],
      persons: [this.todo.persons]
    });
  }

  public saveTodo(): void {
    this.submitted = true;

    if (this.formGroup.invalid) {
      return;
    }

    Object.assign(this.todo, this.formGroup.value);

    this.saved.emit(this.todo);
  }
}
