import { join } from 'lodash';
import { Person } from './person';

export class Todo {
    public deadLineDate?: Date;
    public personsId: number[];

    constructor(public id?: number, public name?: string, public creationTime?: Date, public isDone?: boolean) {
    }

    private _persons: Person[];

    public get persons(): Person[] {
        return this._persons;
    }
    public set persons(value: Person[]) {
        this._persons = value;
        this.personsId = (value || []).map(t => t.id);
    }

    public get personsAsString(): string {
        return join((this.persons || []).map(p => p.label), ", ");
    }
}
