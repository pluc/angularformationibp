import { Injectable } from '@angular/core';
import { BehaviorSubject, from, Observable, of, scheduled, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICrudService } from 'src/app/services/ICrudService';
import { Todo } from '../models/todo';
import * as _ from 'lodash';

@Injectable()
export class TodoService implements ICrudService<Todo> {
  private _todoList: Todo[] = []

  private _todoList$: BehaviorSubject<Todo[]> = new BehaviorSubject<Todo[]>(this._todoList);

  constructor() {
    this.loadFromLocalStorage();
    this.notifyTodoList();
  }

  public notifyTodoList() {
    // ... => opérateur de propagation pour couper la référence du tableau
    this._todoList$.next([...this._todoList]);
  }

  GetAll(): Observable<Todo[]> {
    return this._todoList$.asObservable();
  }

  Create(item: Todo): Observable<boolean> {
    const observable1$: Observable<boolean> = of(true);

    // pipe permets d'enchaine une série d'opérateurs sur les observables
    const res$: Observable<boolean> = observable1$.pipe(

      // premier enchainement : on va logguer le résultat de l'observable 1
      map(o1 => {
        console.log('Valeur de observable1$ : ' + o1);
        return true;
      }),

      // 2ème enchainement, on ajoute l'item au tableau
      map(r => {
        let maxId = _.max(this._todoList.map(t => t.id));
        if (maxId == null) {
          maxId = 0;
        }
        item.id = maxId + 1;
        this._todoList.push(item);
        this.notifyTodoList();
        this.saveInLocalStorage();
        return r;
      })
    )

    // On retourne la suite d'observables a exécuter
    return res$;
  }

  Update(item: Todo): Observable<boolean> {
    const res$: Observable<boolean> = of(true);

    return res$.pipe(
      map(r => {
        const index: number = this._todoList.findIndex(t => t === item);
        this._todoList[index] = item;

        this.notifyTodoList();
        this.saveInLocalStorage();
        return r;
      })
    );
  }

  Delete(item: number | Todo): Observable<boolean> {
    const res$: Observable<boolean> = of(true);

    return res$.pipe(
      map(r => {
        const removedElements: Todo[] = _.remove(this._todoList, t => t === item);
        const success = removedElements && removedElements.length === 1;

        if (success) {
          this.notifyTodoList();
          this.saveInLocalStorage();
        }

        return success;
      })
    );
  }

  Get(id: number): Observable<Todo> {
    throw new Error('Method not implemented.');
  }

  public getTodo(id: number): Observable<Todo> {
    const todo: Todo = this._todoList.find(t => t.id == id);

    const todo$: Observable<Todo> = of(todo);

    return todo$;
  }


  private saveInLocalStorage(): void {
    localStorage.setItem('todos', JSON.stringify(this._todoList));
  }

  private loadFromLocalStorage(): void {
    const todosAsObjects: any[] = JSON.parse(localStorage.getItem('todos')) || [];

    const todoList: Todo[] = [];

    // On instancie des Todo pour chaque objet todo trouvé
    todosAsObjects.map((todoAsObject: any) => {
      const todo = new Todo();
      Object.assign(todo, todoAsObject);
      todoList.push(todo);
    })

    this._todoList = todoList;
  }
}
