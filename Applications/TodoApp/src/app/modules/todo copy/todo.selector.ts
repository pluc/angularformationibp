import { createFeatureSelector } from '@ngrx/store';
import { todoListFeatureKey, TodoListState } from './todos.reducer';

export const todoListStateSelector$ = createFeatureSelector<TodoListState>(todoListFeatureKey);

