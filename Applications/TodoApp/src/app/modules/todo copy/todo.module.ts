import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { TodoService } from './services/todo.service';
import { CreateTodoComponent } from './components/create-todo/create-todo.component';
import { UpdateTodoComponent } from './components/update-todo/update-todo.component';
import { CreateUpdateTodoComponent } from './components/create-update-todo/create-update-todo.component';
import { PersonService } from './services/person.service';
import { EffectsModule } from '@ngrx/effects';
import { TodoListEffects } from './todos.effects';

@NgModule({
  declarations: [
    TodoListComponent,
    CreateTodoComponent,
    UpdateTodoComponent,
    CreateUpdateTodoComponent
  ],
  imports: [
    SharedModule
  ],
  exports: [
    TodoListComponent
  ],
  providers: [
    TodoService,
    PersonService,
  ]
})
export class TodoCopyModule { }
