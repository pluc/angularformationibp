import { Action, createReducer, on } from '@ngrx/store';
import { Todo } from '@todo-ngrx/models';
import * as TodoListActions from '@todo-ngrx/todo.actions';

export const todoListFeatureKey = 'TodoListFeature';

export class TodoListState {
    public selectedTodo?: Todo;
    constructor(public entities: Todo[], public isFromEffect?: boolean) { }
}

export const initialState = new TodoListState([]);

const _todoListReducer = createReducer(
    initialState,
    on(TodoListActions.loadTodoListAction, (state: TodoListState) => { return state; }),
    on(TodoListActions.loadTodoListSuccessAction, (state: TodoListState, { todos }) => {
        return new TodoListState(todos);
    }),
    on(TodoListActions.updateTodoIsDoneAction, (state: TodoListState) => { return state; })
);

export function todoListReducer(state: TodoListState, action: Action) {
    return _todoListReducer(state, action);
}
