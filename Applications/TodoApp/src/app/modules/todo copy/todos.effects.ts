import { Injectable } from "@angular/core";
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Todo } from '@todo-ngrx/models';
import { TodoService } from '@todo-ngrx/services/todo.service';
import * as TodoListActions from '@todo-ngrx/todo.actions';
import { Observable } from 'rxjs';
import { exhaustMap, map } from 'rxjs/operators';

@Injectable()
export class TodoListEffects {
    @Effect()
    LoadTodos$: Observable<Action> = this.actions$.pipe(
        ofType(TodoListActions.loadTodoListAction),
        exhaustMap((action: Action) => {
            console.info('LoadTodos$ Effect - mergeMap', action);
            return this.todoService.GetAll();
        }),
        map((todos: Todo[]) => {
            console.info('LoadTodos$ Effect - map', todos);
            return TodoListActions.loadTodoListSuccessAction({ todos: todos });
        }) 
    );

    constructor(private todoService: TodoService, private actions$: Actions) { }
}
