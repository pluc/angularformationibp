import { ChangeDetectionStrategy, Component, DoCheck, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Store } from '@ngrx/store';
import { todoListStateSelector$ } from '@todo-ngrx/todo.selector';
import { TodoListState } from '@todo-ngrx/todos.reducer';
import { Observable } from 'rxjs';
import { AppState } from 'src/app/reducers';
import { Todo } from '../../models/todo';
import { CreateTodoComponent } from '../create-todo/create-todo.component';
import { UpdateTodoComponent } from '../update-todo/update-todo.component';
import * as TodoListActions from '@todo-ngrx/todo.actions';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoListComponent implements OnInit, DoCheck {

  public message: string;
  private currentTimeout: any;
  public alertType: string;

  public todoListState$: Observable<TodoListState>;

  constructor(
    private store: Store<AppState>,
    private modalService: NgbModal) {

    this.todoListState$ = this.store.select(todoListStateSelector$);

    // this.todoListState$.subscribe(s => {
    //   console.info(s);
    // });
  }

  ngDoCheck(): void {
    console.info('TodoListComponent - DoCheck()');
  }

  ngOnInit(): void {
    this.store.dispatch(TodoListActions.loadTodoListAction());

    console.log('Component TodoList initializing');

  }

  public updateTodo(todo: Todo): void {

  }

  public updateTodoInModal(todo: Todo): void {
    this.modalService.open(UpdateTodoComponent).componentInstance.todo = todo;
  }

  public removeTodo(todo: Todo): void {

  }

  public addTodo(): void {
    this.modalService.open(CreateTodoComponent);
  }

  private showAlertMessage(s: string, success: boolean): void {
    this.message = s;

    if (this.currentTimeout) {
      clearTimeout(this.currentTimeout);
    }

    this.alertType = success ? 'success' : 'danger';

    this.currentTimeout = setTimeout(() => this.message = null, 2000);
  }
}
