import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Todo } from '../../models/todo';
import { TodoService } from '../../services/todo.service';

@Component({
  selector: 'app-update-todo',
  templateUrl: './update-todo.component.html',
  styleUrls: ['./update-todo.component.scss']
})
export class UpdateTodoComponent implements OnInit {
  public hasErrors: boolean;

  @Input()
  public todo: Todo;

  constructor(private todoService: TodoService, private ngbActiveModal: NgbActiveModal) { }

  ngOnInit(): void {
  }

  public saveTodo(todo: Todo): void {
    this.todoService.Update(todo).subscribe(
      success => {
        this.hasErrors = false;
        if (this.ngbActiveModal) {
          this.ngbActiveModal.close();
        }
      },
      error => {
        this.hasErrors = true;
      },
      () => { }
    );
  }

}
