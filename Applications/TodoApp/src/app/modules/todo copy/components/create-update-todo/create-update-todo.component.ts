import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Person, Todo } from '@todo-ngrx/models';
import { PersonService } from '@todo-ngrx/services/person.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-create-update-todo',
  templateUrl: './create-update-todo.component.html',
  styleUrls: ['./create-update-todo.component.scss']
})
export class CreateUpdateTodoComponent implements OnInit {

  @Input()
  public todo: Todo;

  @Input()
  public hasErrors: boolean;

  @Output()
  public saved = new EventEmitter<Todo>();

  public formGroup: FormGroup;
  public submitted: boolean;
  public personList$: Observable<Person[]>;

  constructor(private formBuilder: FormBuilder, private personService: PersonService) { }

  ngOnInit(): void {
    this.personList$ = this.personService.GetAll();

    this.formGroup = this.formBuilder.group({
      name: [this.todo.name, Validators.required],
      deadLineDate: [this.todo.deadLineDate],
      persons: [this.todo.persons]
    });
  }

  public saveTodo(): void {
    this.submitted = true;

    if (this.formGroup.invalid) {
      return;
    }

    Object.assign(this.todo, this.formGroup.value);

    this.saved.emit(this.todo);
  }
}
