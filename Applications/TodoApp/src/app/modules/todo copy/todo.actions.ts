import { createAction, props } from '@ngrx/store';
import { Todo } from './models';

export enum TodoListActionsTypes {
    LoadTodoList = 'LoadTodoList',
    LoadTodoListSuccess = 'LoadTodoListSuccess',
    UpdateTodoIsDone = 'UpdateTodoIsDone'
}

export const loadTodoListAction = createAction(TodoListActionsTypes.LoadTodoList);

export const loadTodoListSuccessAction = createAction(TodoListActionsTypes.LoadTodoListSuccess, props<{ todos: Todo[] }>());

export const updateTodoIsDoneAction = createAction(TodoListActionsTypes.UpdateTodoIsDone, props<{ todo: Todo }>());

