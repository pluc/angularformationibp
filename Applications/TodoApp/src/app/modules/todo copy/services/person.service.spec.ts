import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing';
import { fakeAsync, inject, TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Person } from '../../todo/models';
import { PersonService } from './person.service';

fdescribe('PersonService', () => {
  let service: PersonService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PersonService]
    });
    service = TestBed.inject(PersonService);
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have 0 person', inject(
    [HttpTestingController],
    (httpMock: HttpTestingController) => {
      let personList: Person[];

      service.GetAll().subscribe(p => personList = p);

      const request: TestRequest = httpMock.expectOne('/assets/data.json');
      request.flush({ persons: [] });

      expect(personList.length).toBe(0);

      httpMock.verify();
    }));

  it('should have 1 person', fakeAsync(inject(
    [HttpTestingController],
    (httpMock: HttpTestingController) => {
      let personList: Person[];

      service.GetAll().subscribe(p => personList = p);

      const request: TestRequest = httpMock.expectOne('/assets/data.json');
      request.flush({ persons: [{ id: 1, label: 'Mock 1' }] });

      expect(personList).toContain({ id: 1, label: 'Mock 1' });

      httpMock.verify();
    })));
});
