import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ICrudService } from 'src/app/services/ICrudService';
import { Person } from '../models/person';

@Injectable()
export class PersonService implements ICrudService<Person> {

  constructor(private httpClient: HttpClient) { }

  GetAll(): Observable<Person[]> {
    return this.httpClient.get('/assets/data.json').pipe(
      map((json: any) => json.persons as Person[]),
      catchError((e, j) => {
        console.info('Error occured while loading persons', e);
        return [];
      })
    );
  }
  Create(item: Person): Observable<boolean> {
    throw new Error('Method not implemented.');
  }
  Update(item: Person): Observable<boolean> {
    throw new Error('Method not implemented.');
  }
  Delete(item: number | Person): Observable<boolean> {
    throw new Error('Method not implemented.');
  }
  Get(id: number): Observable<Person> {
    throw new Error('Method not implemented.');
  }
}
