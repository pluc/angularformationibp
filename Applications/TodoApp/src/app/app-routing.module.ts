import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CounterComponent } from '@counter/counter/counter.component';
import { HomeComponent } from './components/home/home.component';
import { TodoListComponent } from './modules/todo/components/todo-list/todo-list.component';
import { TodoListComponent as TodoListComponentCopy } from './modules/todo copy/components/todo-list/todo-list.component';

const routes: Routes = [
  { path: 'todoList', component: TodoListComponent },
  { path: 'todoListCopy', component: TodoListComponentCopy },
  { path: 'home', component: HomeComponent },
  { path: 'counter', component: CounterComponent },
  { path: '**', redirectTo: 'home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
