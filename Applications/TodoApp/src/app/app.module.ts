import { registerLocaleData } from '@angular/common';
import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoModule } from './modules/todo/todo.module';
import localeFr from '@angular/common/locales/fr';
import { HomeComponent } from './components/home/home.component';
import { MenuComponent } from './components/menu/menu.component';
import { SharedModule } from './modules/shared/shared.module';
import { CounterModule } from './modules/counter/counter.module';
import { CounterExampleComponent } from './components/counter-example/counter-example.component';
import { TodoCopyModule } from './modules/todo copy/todo.module';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './app.effects';
import { TodoListEffects } from '@todo-ngrx/todos.effects';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MenuComponent,
    CounterExampleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TodoModule,
    TodoCopyModule,
    SharedModule,
    CounterModule,
    StoreModule.forRoot(reducers),
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    EffectsModule.forRoot([AppEffects, TodoListEffects])
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
